class List:
    def __init__(self, value, next_=None):
        self._value = value
        self._next = next_

    def print(self):
        print(self._value)
        obj = self

        while obj._next is not None:
            obj = obj._next
            print(obj._value)

    def _return_last(self):
        obj = self
        while obj._next is not None:
            obj = obj._next
        return obj

    def append(self, value):
        obj = self._return_last()
        obj._next = List(value)

    def __iadd__(self, other):
        try:
            obj = self._return_last()
            for i in range(len(other)):
                obj._next = List(other.__getitem__(i))
                obj = obj._next
            return self
        except AttributeError:
            return self
        except TypeError:
            return self

    def __iter__(self):
        obj = self
        while obj is not None:
            yield obj._value
            obj = obj._next

    def print_reversed(self):
        obj = self

        if obj._next is not None:
            obj._next.print_reversed()
        print(self._value)

    def __getitem__(self, item):
        try:
            obj = self
            for i in range(item):
                if obj._next is not None:
                    obj = obj._next

            return obj._value
        except TypeError:
            return None

    def __len__(self):
        length = 1
        obj = self
        while obj._next is not None:
            length += 1
            obj = obj._next
        return length

