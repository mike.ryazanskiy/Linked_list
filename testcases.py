from main import List
import sys


def test():
    print('Test print:')
    test_print()
    print('\nTest append:')
    test_append()
    print('\nTest shallow copy:')
    test_shallow_copy()
    print('\nTest change tail value:')
    test_change_value()
    print('\nTest adding list type tail:')
    test_add_list_type_tail()
    print('\nTest iterate object:')
    test_iterate_object()
    print('\nTest reversed print:')
    test_reverse_print()


def test_print():
    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #1 : ', 'Ok' if text == '1\n2\n3\n' else 'False')

    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1)
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #2 : ', 'Ok' if text == '1\n' else 'False')

    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=None, next_=List(value=1, next_=None))
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #3 : ', 'Ok' if text == 'None\n1\n' else 'False')


def test_append():
    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        list_.append(4)
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #1 : ', 'Ok' if text == '1\n2\n3\n4\n' else 'False')

    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        list_.append(4)
        list_.append(5)
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #2 : ', 'Ok' if text == '1\n2\n3\n4\n5\n' else 'False')


def test_shallow_copy():
    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        tail = List(value=4, next_=List(value=5))
        list_ += tail
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #1 : ', 'Ok' if text == '1\n2\n3\n4\n5\n' else 'False')


def test_change_value():
    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        tail = List(value=4, next_=List(value=5))
        list_ += tail
        tail._value = 0
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #1 : ', 'Ok' if text == '1\n2\n3\n4\n5\n' else 'False')


def test_add_list_type_tail():
    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        list_ += [4, 5]
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #1 : ', 'Ok' if text == '1\n2\n3\n4\n5\n' else 'False')

    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        list_ += []
        list_.print()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #2 : ', 'Ok' if text == '1\n2\n3\n' else 'False')


def test_iterate_object():
    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        for elem in list_:
            print(2 ** elem)

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #1 : ', 'Ok' if text == '2\n4\n8\n' else 'False')

    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        for elem in list_:
            print(elem)

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #2 : ', 'Ok' if text == '1\n2\n3\n' else 'False')


def test_reverse_print():
    with open('test_output.txt', 'w') as file:
        old_stdout = sys.stdout
        sys.stdout = file

        list_ = List(value=1, next_=List(value=2, next_=List(value=3)))
        list_.print_reversed()

        sys.stdout = old_stdout

    with open('test_output.txt') as file:
        text = file.read()
        print('Test case #1 : ', 'Ok' if text == '3\n2\n1\n' else 'False')


if __name__ == "__main__":
    test()
