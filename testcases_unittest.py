import unittest
from unittest.mock import patch
from main import List
from io import StringIO


class LinkedListTestCases(unittest.TestCase):
    def setUp(self):
        self.list_ = List(value=1, next_=List(value=2, next_=List(3)))
        self.tail = List(value=4, next_=List(5))

    def usual_case(self, value):
        with patch('sys.stdout', new=StringIO()) as fakeOutput:
            self.list_.print()
            self.assertEqual(fakeOutput.getvalue().strip(), value)

    def test_print(self):
        self.usual_case('1\n2\n3')

    def test_append(self):
        self.list_.append(4)
        self.usual_case('1\n2\n3\n4')

    def test_shallow_copy(self):
        self.list_ += self.tail
        self.usual_case('1\n2\n3\n4\n5')

    def test_change_tail(self):
        self.list_ += self.tail
        self.tail._value = 0
        self.usual_case( '1\n2\n3\n4\n5')

    def test_list_type_tail(self):
        self.list_ += [4, 5]
        self.usual_case('1\n2\n3\n4\n5')

    def test_iter_obj(self):
        with patch('sys.stdout', new=StringIO()) as fakeOutput:
            for elem in self.list_:
                print(2 ** elem)
            self.assertEqual(fakeOutput.getvalue().strip(), '2\n4\n8')

    def test_print_reversed(self):
        with patch('sys.stdout', new=StringIO()) as fakeOutput:
            self.list_.print_reversed()
            self.assertEqual(fakeOutput.getvalue().strip(), '3\n2\n1')


if __name__ == "__main__":
    unittest.main(module=__name__, buffer=True)